## Synopsis

Program similar to ls -lR that outputs the contents into text, json, or yaml.


## Code Example

Simply run the application on the specified directory and the file/folder contents will be output to standard out.

##--Arguements:

--recursive (optional)
--path=/path/to/directory(Required)
--help - Displays a list of commands
--output=

### text
    .idea/
    FileLister.iml
    bin/
    readme.md
    src/


### yaml
    -
    ModifiedTime: "2016-02-08 19:17:08.121169702-0800 PST"
    IsDir: true
    IsLink: false
    LinksTo: ""
    Size: 4096
    Name: ".idea"
    Children: []
    -
    ModifiedTime: "2016-02-08 14:13:48.681912328-0800 PST"
    IsDir: false
    IsLink: false
    LinksTo: ""
    Size: 361
    Name: "FileLister.iml"
    Children: []

### json

    [{
        "ModifiedTime": "2016-02-08 19:17:08.121169702 -0800 PST",
        "IsDir": true,
        "IsLink": false,
        "LinksTo": "",
        "Size": 4096,
        "Name": ".idea",
        "Children": null
    },
    {
        "ModifiedTime": "2016-02-08 14:13:48.681912328 -0800 PST",
        "IsDir": false,
        "IsLink": false,
        "LinksTo": "",
        "Size": 361,
        "Name": "FileLister.iml",
        "Children": null
    }]

## Installation

Just pull the project

1) Clone the repo
2) mkdir FileLister/bin
3) go build bin/FileLister FileLister
4) ./bin/FileLister --help to verify that it works

## Contributors

Chris Stalligns
crstalli@syr.edu