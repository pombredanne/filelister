package main

import (
	"fmt";
	"os"
	"errors"
	"strings"
	"io/ioutil"
	"encoding/json"
	"strconv"
)

/**
 * @Author - Chris Stallings
 * 2/8/2016
 * crstalli@syr.edu
 */


const HELP = "--help"
const RECURSIVE = "--recursive"
const PATH = "--path"
const OUTPUT = "--output"
const OUTPUT_TEXT = "text"
const OUTPUT_JSON = "json"
const OUTPUT_YAML = "yaml"
const DEFAULT_INTEGER_BASE = 10

//Struct representing the output of the typical program case
type Folder struct {
	ModifiedTime string
	IsDir        bool
	IsLink       bool
	LinksTo      string
	Size         int64
	Name         string
	Children     []Folder
}

//Struct representing the valid program arguments
type FileListArgs struct {
	Path      string
	Recursive bool
	Output    string
}

//Map representing a set of the valid flags
var booleanArgs = map[string]bool{
	HELP : true,
	RECURSIVE: true,
}

//Map representing a set of the valid flags where a string value is provided
var stringArgs = map[string]bool{
	PATH: true,
	OUTPUT: true,
}

//Map representing a set of the valid outout flag values
var validOutputTypes = map[string]bool{
	OUTPUT_TEXT : true,
	OUTPUT_JSON : true,
	OUTPUT_YAML : true,
}



func main() {
	//flag package does not support -- arguments
	//The first arguement is the command to run the program(ex ./FileLister)
	args := os.Args[1:len(os.Args)]

	if len(args) == 0 {
		println("No Arguments provided. Run with --help for more information")
		return
	}

	fileListArgs, err := parseArgs(args)
	if fileListArgs == nil && err == nil {
		printHelp()
		return
	}

	if err != nil {
		fmt.Println(err)
	}

	if fileListArgs == nil {
		return
	}
	listFiles(*fileListArgs)
}


/*Parses the array of arguments and returns a set of FileListArgs if
 they are all valid, otherwise returns an error. If both are nil,
 print help
*/
func parseArgs(args []string) (*FileListArgs, error) {

	var validArgs FileListArgs
	var err error

	for i, arg := range args {
		if booleanArgs[arg] {
			if arg == HELP {
				if i != 0 || len(args) > 1 {
					return nil, errors.New("Invalid Arguments: If --help is specified, it can be the only arguement provided.")
				} else {
					return nil, nil
				}
			} else if arg == RECURSIVE {
				validArgs.Recursive = true
			}
		} else {
			argValue := strings.Split(arg, "=")
			if len(argValue) == 1 {
				//Arguement provided that does not have an = or is not one of the boolean arguments
				return nil, errors.New("Arguement does not have an = : " + arg)
			} else {
				if stringArgs[argValue[0]] {
					//Set the param value while joining the rest of the arguments with =
					if value := strings.Join(argValue[1:len(argValue)], "="); argValue[0] == PATH {
						if _, err := os.Stat(value); err == nil {
							validArgs.Path = value
						} else {
							return nil, errors.New("Path does not exist: " + value)
						}
					} else {
						if validOutputTypes[value] {
							validArgs.Output = value
						} else {
							return nil, errors.New("Invalid output type: " + value)
						}
					}
				} else {
					//Arguement with = provided but unknown.
					return nil, errors.New("Arguement value is not provided: " + arg)
				}
			}
		}
	}

	if validArgs.Path == "" {
		return nil, errors.New("Path is a required arguement")
	}

	if validArgs.Output == "" {
		validArgs.Output = "text";
	}

	return &validArgs, err
}

//Prints the help output
func printHelp() {
	fmt.Println("FileLister can take the following arguments ")
	fmt.Println("--help  <prints help>");
	fmt.Println("--path=<path to folder, required>");
	fmt.Println("--recursive  (when set, list files recursively.  default is off)")
	fmt.Println("--output=<json|yaml|text, default is text>")
}

//Returns an array of folder objects depending on if
//we are recursively outputting the objects
func getDirectory(path string, isRecursive bool) ([] Folder, error) {
	var folders []Folder
	files, err := ioutil.ReadDir(path)
	if (err != nil) {
		return nil, err
	}
	for _, f := range files {
		var children []Folder
		if isRecursive {
			if f.IsDir() {
				children, err = getDirectory(path + "/" + f.Name(), isRecursive)
				if err != nil {
					return nil, err
				}
			}
			if children == nil {
				children = []Folder{}
			}
		}
		isLink := (f.Mode() & os.ModeSymlink) == os.ModeSymlink
		var linksTo string
		if isLink {
			linksTo, err = os.Readlink(path + "/" + f.Name())
			if err != nil {
				return nil, err
			}
		}

		folder := Folder{f.ModTime().String(), f.IsDir(), isLink, linksTo, f.Size(), f.Name(), children}
		folders = append(folders, folder)
	}

	return folders, err
}


//Gets the list of Folder objects then calls marshalls
func listFiles(fileListArgs FileListArgs) {
	folders, err := getDirectory(fileListArgs.Path, fileListArgs.Recursive)
	if err != nil {
		fmt.Println(err)
	}
	if len(folders) == 0 {
		fmt.Println("yo")
	}
	output, err := marshall(fileListArgs.Path, folders, fileListArgs.Output)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Printf(output)
}


//Marshalls the different output types
func marshall(path string, files []Folder, outputType string) (string, error) {
	switch outputType {
	case OUTPUT_JSON:
		return marshallJson(files)
	case OUTPUT_YAML:
		return marshallYaml(files)
	case OUTPUT_TEXT:
		return marshallText(path, files)
	default:
		return "", errors.New("Unknown output type" + outputType)
	}
}

//Marshalls the json
func marshallJson(files []Folder) (string, error) {
	b, err := json.MarshalIndent(files, "", "    ")
	if err != nil {
		return "", err
	}
	return string(b) + "\n", nil
}

//Marshalls the yaml
func marshallYaml(files []Folder) (string, error) {
	return marshallInnerYaml(files, "  "), nil
}

//Recursively Marshalls the inner yaml
func marshallInnerYaml(files []Folder, indent string) (string) {
	val := ""
	children := ""
	preArrIndent := indent
	indent += "  "
	for _, f := range files {
		val += preArrIndent + "- \n"
		val += indent + "ModifiedTime: \"" + f.ModifiedTime + "\"\n"
		val += indent + "IsDir: " + strconv.FormatBool(f.IsDir) + "\n"
		val += indent + "IsLink: " + strconv.FormatBool(f.IsLink) + "\n"
		val += indent + "LinksTo: \"" + f.LinksTo + "\"\n"
		val += indent + "Size: " + strconv.FormatInt(f.Size, DEFAULT_INTEGER_BASE) + "\n"
		val += indent + "Name: \"" + f.Name + "\"\n"
		if f.Children == nil || len(f.Children) == 0 {
			children = "[]\n"
		} else {
			children = "\n" + marshallInnerYaml(f.Children, indent + "  ")
		}
		val += indent + "Children: " + children
	}
	return val
}

//Recursively the text output
func marshallText(path string, files []Folder) (string, error) {
	text := path + marshallInnerText(files, "  ") + "\n"
	return text, nil
}

//Recursively marshalls the the text arguement
func marshallInnerText(files []Folder, indent string) string {
	val := ""
	for _, f := range files {
		val += "\n" + indent + f.Name
		if f.IsLink {
			val += "* (" + f.LinksTo + ")"
		}
		if f.IsDir {
			val += "/"
		}
		if f.Children != nil {
			marshallInnerText(f.Children, indent + "  ")
		}
	}
	return val
}
